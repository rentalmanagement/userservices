package com.rental.management.userservices.model;

public enum UserType
{
    LANDLORD,
    RENTER,
    NONE
}
