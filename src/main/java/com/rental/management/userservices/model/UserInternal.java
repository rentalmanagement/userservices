package com.rental.management.userservices.model;

import javax.persistence.*;

@Entity
@Table(name = "users",uniqueConstraints = @UniqueConstraint(name = "uq_username"
        ,columnNames = "username"))
public class UserInternal
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id" ,unique = true)
    private Long id;
    @Column(name = "username",nullable = false)
    private String username;
    @Column(name = "password",nullable = false)
    private String password;
    @Column(name = "phoneNumber" , nullable = false)
    private String phoneNumber;
    @Column(name = "emailAddress", nullable = false)
    private String emailAddress;
    @Column(length = 32, columnDefinition = "varchar(32) default 'NONE'")
    @Enumerated(value = EnumType.STRING)
    private UserType userType;

    public UserInternal()
    {
    }

    public UserInternal(String username, String password, String phoneNumber, String emailAddress,UserType userType)
    {
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.userType = userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public UserType getUserType()
    {
        return userType;
    }

    public void setUserType(UserType userType)
    {
        this.userType = userType;
    }

    @Override
    public String toString()
    {
        return "UserInternal{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
