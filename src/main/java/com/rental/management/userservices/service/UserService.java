package com.rental.management.userservices.service;

import com.rental.management.userservices.Utils.UserUtils;
import com.rental.management.userservices.model.User;
import com.rental.management.userservices.model.UserInternal;
import com.rental.management.userservices.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;


@Component
public class UserService
{
    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public User getById(Long id)
    {
       return UserUtils.getUserFromUserInternal(userRepository.getOne(id));
    }

    @Transactional
    public List<User> getAll()
    {
      return UserUtils.getUserFromUserInternal(userRepository.findAll());
    }

    @Transactional
    public User checkLogin(String username,String password)
    {
        List<UserInternal> userInternals = userRepository.findUserInternalByUsernameAndPassword(username,password);
        if(userInternals.size()==1)
        {
            if(password.compareTo(userInternals.get(0).getPassword())==0)
                return  UserUtils.getUserFromUserInternal(userInternals.get(0));
        }
        return null;
    }

    @Transactional
    public User getByUsername(String username)
    {
        UserInternal userInternals = userRepository.findUserInternalByUsername(username);
        return UserUtils.getUserFromUserInternal(userInternals);
    }

    @Transactional
    public User save(User user, String password)
    {
        UserInternal userInternal = UserUtils.createUserInternalFromUser(user,password);
        UserInternal saved=userRepository.save(userInternal);
        return UserUtils.getUserFromUserInternal(saved);
    }

    @Transactional// take a look for ex
    public User update(User user,String newPassword)//"" newPassword means it is not changed
    {
        System.out.println(user);
        Optional<UserInternal> userInternalOptional = userRepository.findById(user.getId());
        if(userInternalOptional.isPresent())
        {
            UserInternal userInternal = userInternalOptional.get();
            userInternal.setUsername(user.getUsername());
            userInternal.setPhoneNumber(user.getPhoneNumber());
            userInternal.setEmailAddress(user.getEmailAddress());
            userInternal.setId(user.getId());
            userInternal.setUserType(user.getUserType());
            if(newPassword!=null&&newPassword.length()>0)
            {
                userInternal.setPassword(newPassword);
            }
           return UserUtils.getUserFromUserInternal( userRepository.save(userInternal));
        }
        return null;
    }


    @Transactional
    public void delete(Long id)
    {
        userRepository.deleteById(id);
    }
}
