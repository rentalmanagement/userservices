package com.rental.management.userservices.Utils;

import com.rental.management.userservices.model.User;
import com.rental.management.userservices.model.UserInternal;

import java.util.ArrayList;
import java.util.List;

public class UserUtils
{
    public static User getUserFromUserInternal(UserInternal userInternal)
    {
        if(userInternal!=null)
        {
            User user = new User();
            user.setId(userInternal.getId());
            user.setUsername(userInternal.getUsername());
            user.setEmailAddress(userInternal.getEmailAddress());
            user.setPhoneNumber(userInternal.getPhoneNumber());
            user.setUserType(userInternal.getUserType());
            return user;
        }
        return null;
    }
    public static List<User> getUserFromUserInternal(List<UserInternal>userInternals)
    {
        List<User> users = new ArrayList<>();
        for(UserInternal userInternal:userInternals)
        {
            users.add(getUserFromUserInternal(userInternal));
        }
        return users;
    }

    public static UserInternal createUserInternalFromUser(User user, String password)
    {
        if(user!=null)
        {
            UserInternal userInternal = new UserInternal();
            userInternal.setUsername(user.getUsername());
            userInternal.setEmailAddress(user.getEmailAddress());
            userInternal.setPhoneNumber(user.getPhoneNumber());
            userInternal.setId(user.getId());
            userInternal.setUserType(user.getUserType());
            userInternal.setPassword(password);
            return userInternal;
        }
        return null;
    }
}
