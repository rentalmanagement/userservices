package com.rental.management.userservices.controller;

import com.rental.management.userservices.model.Authentication;
import com.rental.management.userservices.model.User;
import com.rental.management.userservices.model.UserDTO;
import com.rental.management.userservices.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/users")
public class UserController
{
    @Autowired
    private UserService userService;

    @PostMapping("/save")
    public User save(@RequestBody UserDTO authentication)
    {
        System.out.println("SAVE");
        return userService.save(authentication.getUser(),authentication.getPassword());
    }

    @PostMapping("/login")
    public User login(@RequestBody Authentication authentication)
    {
        User user = userService.checkLogin(authentication.getUsername(),authentication.getPassword());
        return user;
    }


    @PutMapping("/update")
    public User update(@RequestBody UserDTO authentication)//"" empty string if password is not changed
    {
        return userService.update(authentication.getUser(),authentication.getPassword());
    }

    @GetMapping("/one/{id}")
    public User getUserById(@PathVariable("id") Long id)
    {
        return userService.getById(id);
    }

    @GetMapping
    public User getUserById(@RequestParam("username") String username)
    {
        return userService.getByUsername(username);
    }

    @GetMapping("/all")
    public List<User> getAll()
    {
        return userService.getAll();
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id)
    {
        userService.delete(id);
    }

}
