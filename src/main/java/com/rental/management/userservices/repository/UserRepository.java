package com.rental.management.userservices.repository;

import com.rental.management.userservices.model.UserInternal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<UserInternal,Long>
{
    UserInternal findUserInternalByUsername(String username);
    List<UserInternal> findUserInternalByUsernameAndPassword(String username,String password);
}
